﻿using System;
using System.Collections.Generic;
using Bachelor_thesis_generator.Cards;

namespace Simulator.DataStructures
{
	public class EncounterResult
	{
		public string ScenarioId { get; }
		public int EncounterNumber { get; }
		public int GameCardSeed { get; }
		public Type BotType { get; }
		public int? AiSeed { get; }
		public IList<Card> Deck { get; }
		public IList<Enemy> EncounterEnemies { get; }
		public bool Won { get; }
		public int RemainingHp { get; }
		public int Turns { get; }
		public IList<Card> CardsPlayed { get; }
		public int TreeId { get; set; }
		public List<int> NodeIds { get; set; }

		public EncounterResult(string scenarioId, Type botType, int? aiSeed, IList<Card> deck,
			IList<Enemy> encounterEnemies, bool won, int remainingHp, int turns, IList<Card> cardsPlayed,
			List<int> nodeIds, int encounterNumber, int gameCardSeed)
		{
			ScenarioId = scenarioId;
			BotType = botType;
			AiSeed = aiSeed;
			Deck = deck;
			EncounterEnemies = encounterEnemies;
			Won = won;
			RemainingHp = remainingHp;
			Turns = turns;
			CardsPlayed = cardsPlayed;
			NodeIds = nodeIds;
			EncounterNumber = encounterNumber;
			GameCardSeed = gameCardSeed;
		}
	}
}
