﻿using System.Collections.Generic;

namespace Simulator.DataStructures
{
	public class SimulationResult
	{
		public IList<EncounterResult> EncounterResults { get; set; } = new List<EncounterResult>(200000);
	}
}
