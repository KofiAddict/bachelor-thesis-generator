﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bachelor_thesis_generator.Cards;
using Bachelor_thesis_generator.DataStructure;
using Simulator.Bots;
using Simulator.DataStructures;

namespace Simulator
{
	public class Simulator
	{
		private Random random = new Random(Seed: 142857);
		private Random cardSeedRandom = new Random(Seed: 758241);
		private List<Task> _tasks = new List<Task>(10000);
		private object resultListLock = new object();
		private object getBotsLock = new object();
		private List<List<int>> _combinations = new List<List<int>>(200000);

		public SimulationResult Simulate(List<Card> cards, NodeTree tree, List<List<Enemy>> enemySets,
			string scenarioId)
		{
			var newResult = new SimulationResult();

			SimulateRecursive(cards, new List<Node>() {tree.Root}, enemySets, newResult, scenarioId);
			_tasks.ForEach(x => x.Wait());

			return newResult;
		}

		private List<Bot> GetBots(List<int> nodeIds)
		{
			return new List<Bot>
			{
				new BestTurnBot(nodeIds),
				new LuckyBot(nodeIds),
				new GreedyBot(nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RouletteBot(random.Next(), nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds),
				new RngBot(random.Next(),nodeIds)
			}; //possibly thread unsafe
		}

		private void SimulateRecursive(List<Card> cards, List<Node> choicesSoFar, List<List<Enemy>> enemySets,
			SimulationResult results, string scenarioId)
		{
			if (choicesSoFar.Count > enemySets.Count || _combinations.Contains(choicesSoFar.Select(x => x.Id).OrderBy(x => x).ToList(), new UnorderedListComparer()))
			{
				return;
			}

			for (int i = 0; i < 10; i++)
			{
				Task task = Task.Run(() =>
				{
					List<Bot> bots;
					lock (getBotsLock)
					{
						bots = GetBots(choicesSoFar.Select(x => x.Id).ToList());
					}
					int cardSeed = cardSeedRandom.Next();
					foreach (var bot in bots)
					{
						var allCards = cards.Select(x => x.Clone()).ToList();
						allCards.Shuffle(cardSeed);
						foreach (var node in choicesSoFar)
						{
							if (node.Parent == null) continue;
							node.ApplyEffectToCards(allCards);
						}

						var game = new Game(enemySets[choicesSoFar.Count - 1].Select(x => x.Clone()).ToList(), allCards, choicesSoFar.Count, scenarioId, cardSeed);
						var encounterResult = bot.FightEncounter(game);
						lock (resultListLock)
						{
							results.EncounterResults.Add(encounterResult);
							Console.WriteLine("Result count: " + results.EncounterResults.Count + " out of " + _tasks.Count*bots.Count);
						}
					}
				});
				_tasks.Add(task);
			}
			_combinations.Add(choicesSoFar.Select(x => x.Id).OrderBy(x => x).ToList());

			var possibleChoices = choicesSoFar.SelectMany(x => x.Children).Distinct().Except(choicesSoFar).ToList();

			foreach (var possibleChoice in possibleChoices)
			{
				var newChoices = choicesSoFar.ToList();
				newChoices.Add(possibleChoice);

				SimulateRecursive(cards, newChoices, enemySets, results, scenarioId);
			}
		}

		public class UnorderedListComparer : IEqualityComparer<List<int>>
		{
			public bool Equals(List<int> x, List<int> y)
			{
				if (x.Count != y.Count)
				{
					return false;
				}
				for (int i = 0; i < x.Count; i++)
				{
					if (x[i] != y[i])
					{
						return false;
					}
				}
				return true;
			}

			public int GetHashCode(List<int> obj)
			{
				int result = 17;
				for (int i = 0; i < obj.Count; i++)
				{
					unchecked
					{
						result = (int)(result * 23 + obj[i]);
					}
				}
				return result;
			}
		}
	}
}
