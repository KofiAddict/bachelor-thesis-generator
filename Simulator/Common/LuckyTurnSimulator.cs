﻿using Bachelor_thesis_generator.Cards;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simulator.Common
{
	class LuckyTurnSimulator : TurnSimulator
	{
		private List<Card> nextHand;
		private TurnState bestNextTurnState;

		public LuckyTurnSimulator(TurnState startingState, List<Card> nextHand) : base(startingState)
		{
			this.nextHand = nextHand;
		}

		protected override void HandleResult(TurnState state)
		{
			state.Health -= state.Enemies.Where(x => x.Health > 0).Sum(x => x.Attack);
			if (state.Health <= 0)
			{
				return;
			}

			if (BestTurnState == null)
			{
				BestTurnState = state;
			}

			if (BestTurnState > state)
			{
				BestTurnState = state;
			}

			if (state.Enemies.All(x => x.Health <= 0))
			{
				return;
			}

			if (state.Enemies.All(x => x.Health <= 0))
			{
				return;
			}

			foreach (var card in nextHand)
			{
				if (card.CardType == CardType.NonTargetedEffect)
				{
					var nextState = state.Clone();
					var spellCard = card;
					nextState.Hand.Remove(spellCard);

					if (spellCard.CostType == CostType.Energy)
					{
						nextState.Energy -= spellCard.Cost;
					}
					else
					{
						nextState.Mana -= spellCard.Cost;
					}

					switch (spellCard.BaseEffectType)
					{
						case EffectType.Heal:
							nextState.Health = nextState.Health + spellCard.EffectValue > nextState.MaxHealth
								? nextState.MaxHealth
								: nextState.Health + spellCard.EffectValue;
							break;
						case EffectType.Damage:
							foreach (var enemy in nextState.Enemies)
							{
								enemy.Health -= enemy.Health - spellCard.EffectValue < 0
									? enemy.Health
									: spellCard.EffectValue;
							}
							break;
						case EffectType.ReduceDamage:
							foreach (var enemy in nextState.Enemies)
							{
								enemy.Attack -= enemy.Attack - spellCard.EffectValue < 0 ? enemy.Attack : spellCard.EffectValue;
							}
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					nextState.played = card;
					if (bestNextTurnState == null || CompareTo(nextState, bestNextTurnState) < 0)
					{
						BestTurnState = nextState.Parent;
						bestNextTurnState = nextState;
					}
				}
				else
				{
					foreach (var enemy in state.Enemies)
					{
						var nextTarget = enemy.Clone();

						if (nextTarget.Health <= 0) continue;

						switch (card.BaseEffectType)
						{
							case EffectType.Heal:
								break;
							case EffectType.Damage:
								nextTarget.Health -= nextTarget.Health - card.EffectValue < 0
									? nextTarget.Health
									: card.EffectValue;
								break;
							case EffectType.ReduceDamage:
								nextTarget.Attack -= nextTarget.Attack - card.EffectValue < 0 ? nextTarget.Attack : card.EffectValue;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}

						var enemies = state.Enemies.Except(new[] { enemy }).Select(x => x.Clone()).ToList();
						enemies.Add(nextTarget);
						var newHand = state.Hand.ToList();
						newHand.Remove(card);

						var nextState = state.Clone();
						nextState.Hand = newHand;
						nextState.Enemies = enemies;

						if (card.CostType == CostType.Energy)
						{
							nextState.Energy -= card.Cost;
						}
						else
						{
							nextState.Mana -= card.Cost;
						}

						nextState.target = enemy;
						nextState.played = card;
						if (bestNextTurnState == null || CompareTo(nextState, bestNextTurnState) < 0)
						{
							BestTurnState = nextState.Parent;
							bestNextTurnState = nextState;
						}
					}
				}
			}
		}

		public int CompareTo(TurnState contender, TurnState best)
		{
			var enemiesAliveBest = best.Enemies.Count(enemy => enemy.Health > 0);
			var enemiesAliveContender = contender.Enemies.Count(enemy => enemy.Health > 0);

			if (enemiesAliveContender != enemiesAliveBest) return enemiesAliveContender > enemiesAliveBest ? -1 : 1;

			var remainingHealthBest = best.Health - best.Enemies.Where(x => x.Health > 0).Sum(x => x.Attack);
			var remainingHealthContender = contender.Health - contender.Enemies.Where(x => x.Health > 0).Sum(x => x.Attack);

			if (remainingHealthContender / (float)remainingHealthBest > 2 && remainingHealthContender - remainingHealthBest > 10) return 1;
			if (remainingHealthBest / (float)remainingHealthContender > 2 && remainingHealthBest - remainingHealthContender > 10) return -1;

			if (remainingHealthBest - remainingHealthContender > 10 && remainingHealthContender - remainingHealthBest > 10)
			{
				return (int)(best.Enemies.Sum(x => x.Health < 0 ? 0 : x.Health) - (int)contender.Enemies.Sum(x => x.Health < 0 ? 0 : x.Health));
			}
			else
			{
				return remainingHealthContender - remainingHealthBest;
			}
		}
	}
}
