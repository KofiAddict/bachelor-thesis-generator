﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bachelor_thesis_generator.Cards;

namespace Simulator
{
	class Game
	{
		public int Health = 50;
		public int Mana = 10;
		public int Energy = 20;
		public int TurnNumber = 1;
		public Queue<Card> Deck;
		public List<Card> Hand = new List<Card>();
		public List<Card> DiscardPile = new List<Card>();
		public List<Enemy> Enemies;
		public List<Card> CardsPlayed = new List<Card>();
		public List<Card> AllCards;
		public string ScenarioId;
		public int EncounterNum;
		public int CardSeed;

		public Game(List<Enemy> enemies, List<Card> allCards, int encounterNum, string scenarioId, int cardSeed)
		{
			Enemies = enemies;
			AllCards = allCards;
			EncounterNum = encounterNum;
			ScenarioId = scenarioId;
			CardSeed = cardSeed;
			Deck = new Queue<Card>(allCards);
			ReDraw();
		}

		public void EndTurn()
		{
			ResetResources();
			GetDamaged();
			ReDraw();
			TurnNumber++;
		}

		private void ResetResources()
		{
			Mana = 10;
			Energy = 20;
		}

		private void GetDamaged()
		{
			Health -= Enemies.Where(x => x.Health > 0).Sum(x => x.Attack);
		}

		private void ReDraw()
		{
			DiscardPile.AddRange(Hand);
			Hand.Clear();
			for (int i = 0; i < 5; i++)
			{
				Hand.Add(Deck.Dequeue());
			}

			if (Deck.Count == 0)
			{
				DiscardPile.Shuffle();
				Deck = new Queue<Card>(DiscardPile);
				DiscardPile.Clear();
			}
		}

		public void PlayCard(Card card, Enemy target = null)
		{
			if (card.CardType == CardType.TargetedEffect && (target == null || target.Health <= 0))
			{
				throw new Exception(message: "Target invalid");
			}

			if (card.CostType == CostType.Energy)
			{
				if (card.Cost > Energy)
					throw new Exception(message: "Tried playing card without resources");
				Energy -= card.Cost;
			}
			else
			{
				if (card.Cost > Mana)
					throw new Exception(message: "Tried playing card without resources");
				Mana -= card.Cost;
			}

			CardsPlayed.Add(card);

			switch (card.CardType)
			{
				case CardType.TargetedEffect:
					Debug.Assert(target != null, nameof(target) + " != null");
					target.Health -= card.EffectValue;
					break;
				case CardType.NonTargetedEffect:
					Enemies.ForEach(x => x.Health -= card.EffectValue);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			Hand.Remove(card);
			DiscardPile.Add(card);
		}

		public bool? IsOver()
		{
			if (Health < 0)
			{
				return false;
			}

			if (Enemies.All(x => x.Health <= 0))
			{
				return true;
			}

			return null;
		}
	}
}