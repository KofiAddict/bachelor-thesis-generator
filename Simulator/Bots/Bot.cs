﻿using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using MoreLinq;
using Simulator.DataStructures;

namespace Simulator.Bots
{
	abstract class Bot
	{
		protected Bot(List<int> nodeIds)
		{
			this.nodeIds = nodeIds;
		}

		public List<int> nodeIds { get; protected set; }
		protected abstract int AiSeed { get; }
		public abstract EncounterResult FightEncounter(Game game);

		protected List<Card> GetPlayableCards(Game game)
		{
			return game.Hand.Where(x => x.CostType == CostType.Energy ? x.Cost <= game.Energy : x.Cost <= game.Mana)
				.ToList();
		}

		protected List<Card> RankCards(List<Card> cards, Game game)
		{
			cards = cards.OrderBy(card => card.CardType == CardType.NonTargetedEffect
					? game.Enemies.Where(x => x.Health > card.EffectValue).Sum(x => x.Attack)
					: game.Enemies.Sum(x => x.Attack) - (game.Enemies.Where(x => x.Health < card.EffectValue)
						                                     .MaxBy(x => x.Attack).FirstOrDefault()?.Attack ?? 0))
				.ThenBy(x => x.EffectValue / (double) x.Cost).ToList();
			return cards;
		}

		protected virtual EncounterResult GenerateResult(Game game)
		{
			return new EncounterResult(game.ScenarioId, this.GetType(), AiSeed,
				// ReSharper disable twice PossibleInvalidOperationException
				game.AllCards, game.Enemies, game.IsOver().Value, game.Health,
				game.IsOver().Value ? game.TurnNumber : game.TurnNumber - 1, game.CardsPlayed, nodeIds, game.EncounterNum, game.CardSeed);
		}
	}
}
