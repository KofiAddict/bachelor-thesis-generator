﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using MoreLinq;
using Simulator.Common;
using Simulator.DataStructures;

namespace Simulator.Bots
{
	class RouletteBot : Bot
	{
		private readonly Random _random;

		public RouletteBot(int seed, List<int> nodeIds) : base(nodeIds)
		{
			AiSeed = seed;
			_random = new Random(seed);
		}

		protected override int AiSeed { get; }

		public override EncounterResult FightEncounter(Game game)
		{
			while (true) //turns
			{
				var state = game.IsOver();

				if (state != null)
				{
					if (state == true) throw new WtfException();
					return GenerateResult(game);
				}

				while (true) //actions
				{
					var cards = GetPlayableCards(game);

					if (cards.Count == 0)
					{
						game.EndTurn();
						break;
					}

					cards = RankCards(cards, game);
					var chosenCard = cards[Math.Clamp((int)-Math.Log(_random.NextDouble(), newBase: 2), 0, cards.Count-1)]; // this should choose 1st option with 50% chance, 2nd with 25% and so on
					var enemies = game.Enemies.Where(x => x.Health > 0).ToList();
					var target = chosenCard.CardType == CardType.TargetedEffect
						? enemies.Where(x => x.Health < chosenCard.EffectValue)
							  .MaxBy(x => x.Attack).FirstOrDefault() ?? enemies.MaxBy(x => x.Attack / x.Health).First()
						: null;

					game.PlayCard(chosenCard, target);

					state = game.IsOver();

					if (state == null) continue;
					if (state == false) throw new WtfException();
					return GenerateResult(game);
				}
			}
		}
	}
}
