﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using Simulator.Common;
using Simulator.DataStructures;

namespace Simulator.Bots
{
	class RngBot : Bot
	{
		private Random _random;

		public RngBot(int seed, List<int> nodeIds) : base(nodeIds)
		{
			AiSeed = seed;
			_random = new Random(seed);
		}

		protected override int AiSeed { get; }

		public override EncounterResult FightEncounter(Game game)
		{
			while (true) //turns
			{
				var state = game.IsOver();

				if (state != null)
				{
					if (state == true) throw new WtfException();
					return GenerateResult(game);
				}

				while (true) //actions
				{
					var cards = GetPlayableCards(game);

					if (cards.Count == 0)
					{
						game.EndTurn();
						break;
					}

					var index = cards.Count > 3 ? _random.Next(cards.Count) : _random.Next(cards.Count + 1);
					if (index == cards.Count)
					{
						game.EndTurn();
						break;
					}
					var chosenCard = cards[index];
					var enemies = game.Enemies.Where(x => x.Health > 0).ToList();
					var target = chosenCard.CardType == CardType.TargetedEffect
						? enemies[_random.Next(enemies.Count)]
						: null;

					game.PlayCard(chosenCard, target);

					state = game.IsOver();

					if (state == null) continue;
					if (state == false) throw new WtfException();
					return GenerateResult(game);
				}
			}
		}
	}
}
