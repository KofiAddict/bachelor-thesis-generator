﻿using System;
using System.IO;
using System.Linq;
using CsvHelper;
using MoreLinq;
using Simulator.DataStructures;

namespace Simulator
{
	public class CsvExport
	{
		public void Export(SimulationResult result, string filePath)
		{
			var csvHelper = new CsvSerializer(new StreamWriter(filePath));
			csvHelper.Configuration.Delimiter = ";";

			result.EncounterResults.Select(x => new string[] { x.ScenarioId.ToString(), x.EncounterNumber.ToString() ,x.BotType.ToString(),
				x.AiSeed.ToString(), x.GameCardSeed.ToString(), x.TreeId.ToString(), x.NodeIds.Select(y => y.ToString()).Aggregate((s, s1) => s+" "+s1), x.Won ? "1" : "0",
				x.RemainingHp.ToString(), x.Turns.ToString(), x.CardsPlayed.Count.ToString(),
				x.EncounterEnemies.Count > 0 ? x.EncounterEnemies[0].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 0 ? x.EncounterEnemies[0].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 1 ? x.EncounterEnemies[1].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 1 ? x.EncounterEnemies[1].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 2 ? x.EncounterEnemies[2].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 2 ? x.EncounterEnemies[2].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 3 ? x.EncounterEnemies[3].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 3 ? x.EncounterEnemies[3].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 4 ? x.EncounterEnemies[4].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 4 ? x.EncounterEnemies[4].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 5 ? x.EncounterEnemies[5].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 5 ? x.EncounterEnemies[5].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 6 ? x.EncounterEnemies[6].BaseAttack.ToString() : "", x.EncounterEnemies.Count > 6 ? x.EncounterEnemies[6].BaseHealth.ToString() : "",
				x.EncounterEnemies.Count > 0 ? x.EncounterEnemies[0].Attack.ToString() : "", x.EncounterEnemies.Count > 0 ? x.EncounterEnemies[0].Health.ToString() : "",
				x.EncounterEnemies.Count > 1 ? x.EncounterEnemies[1].Attack.ToString() : "", x.EncounterEnemies.Count > 1 ? x.EncounterEnemies[1].Health.ToString() : "",
				x.EncounterEnemies.Count > 2 ? x.EncounterEnemies[2].Attack.ToString() : "", x.EncounterEnemies.Count > 2 ? x.EncounterEnemies[2].Health.ToString() : "",
				x.EncounterEnemies.Count > 3 ? x.EncounterEnemies[3].Attack.ToString() : "", x.EncounterEnemies.Count > 3 ? x.EncounterEnemies[3].Health.ToString() : "",
				x.EncounterEnemies.Count > 4 ? x.EncounterEnemies[4].Attack.ToString() : "", x.EncounterEnemies.Count > 4 ? x.EncounterEnemies[4].Health.ToString() : "",
				x.EncounterEnemies.Count > 5 ? x.EncounterEnemies[5].Attack.ToString() : "", x.EncounterEnemies.Count > 5 ? x.EncounterEnemies[5].Health.ToString() : "",
				x.EncounterEnemies.Count > 6 ? x.EncounterEnemies[6].Attack.ToString() : "", x.EncounterEnemies.Count > 6 ? x.EncounterEnemies[6].Health.ToString() : ""
			}).ForEach(x =>
			{
				csvHelper.Write(x);
				csvHelper.WriteLine();
			});

			csvHelper.Dispose();
		}
	}
}
