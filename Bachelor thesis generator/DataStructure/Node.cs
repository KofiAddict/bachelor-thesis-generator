﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;

namespace Bachelor_thesis_generator.DataStructure
{
	public class Node
	{
		public int Id;
		public List<Node> Children = new List<Node>();
		public Action<IEnumerable<Card>> EffectAction => NodeEffects.ChangeFlatEffectAction(NodePower, NodeTargets);
		public Node Parent { get; set; }
		public NodeTarget[] NodeTargets;
		public int Depth;

		public NodeState NodeState { get; set; } = NodeState.None;

		public int NodePower { get; set; }

		public void ApplyEffectToCards(IEnumerable<Card> cards)
		{
			EffectAction.Invoke(cards);
		}

		public Node Clone()
		{
			var node = new Node
			{
				Id = Id,
				Children = Children,
				Parent = Parent,
				NodeTargets = NodeTargets.ToArray(),
				NodeState = NodeState,
				NodePower = NodePower,
				Depth = Depth
			};
			return node;
		}

		public static Node FromEntity(NodeEntity entity)
		{
			var node = new Node
			{
				Id = entity.Id,
				NodePower = entity.EffectValue,
				NodeTargets = entity.Targets
			};
		
			return node;
		}

		public NodeEntity ToEntity()
		{
			return new NodeEntity
			{
				Id = Id,
				ChildrenIds = Children.Select(x => x.Id).ToArray(),
				EffectValue = NodePower,
				Targets = NodeTargets
			};
		}
	}
}
