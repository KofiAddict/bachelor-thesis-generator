﻿using System;
using Bachelor_thesis_generator.Cards;

namespace Bachelor_thesis_generator.DataStructure
{
	[Serializable]
	public class NodeEntity
	{
		public int Id;
		public int EffectValue;
		public NodeTarget[] Targets;
		public int[] ChildrenIds;
	}

	[Serializable]
	public class NodeEntities
	{
		public NodeEntity[] Entities;
	}
}