﻿using System.Linq;
using Bachelor_thesis_generator.DataStructure;
using Newtonsoft.Json;

namespace Bachelor_thesis_generator
{
	public class JsonExporter : IExporter
	{
		public string Export(NodeTree tree)
		{
			var list = tree.NodeList;

			var entities = new NodeEntities
			{
				Entities = list.Select(x => x.ToEntity()).ToArray()
			};

			return JsonConvert.SerializeObject(entities);
		}

		public NodeTree Import(string serialized)
		{
			var entities = JsonConvert.DeserializeObject<NodeEntities>(serialized);

			return NodeTree.FromEntities(entities.Entities);
		}
	}
}
