﻿using Bachelor_thesis_generator.DataStructure;

namespace Bachelor_thesis_generator.Operators
{
	internal interface IOperator
	{
		NodeTree Mutate(NodeTree tree, InputModel inputModel);
	}
}
