﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using Bachelor_thesis_generator.DataStructure;

namespace Bachelor_thesis_generator.Operators
{
	internal class AddNodeOp : IOperator
	{
		private readonly Random random;
		private readonly double prob;

		public AddNodeOp(Random random, double prob)
		{
			this.random = random;
			this.prob = prob;
		}

		public NodeTree Mutate(NodeTree tree, InputModel input)
		{
			if (random.NextDouble() > prob)
			{
				return null;
			}

			var chance = random.NextDouble();
			Node chosen;
			List<Node> nodeList = tree.NodeList.Where(x => x.Children.Count <= input.PowerMarks.Keys.Max() - x.Depth).ToList();
			if (chance < 0.5)
			{
				var children = nodeList.Where(x => x.Children.Count == 0).ToList();
				if (children.Count == 0)
				{
					return null;
				}

				chosen = children[random.Next(children.Count)];
			}
			else if (chance < 0.9)
			{
				var children = nodeList.Where(x => x.Children.Count == 1).ToList();
				if (children.Count == 0)
				{
					return null;
				}

				chosen = children[random.Next(children.Count)];
			}
			else
			{
				var children = nodeList.Where(x => x.Children.Count > 1).ToList();
				if (children.Count == 0)
				{
					return null;
				}

				chosen = children[random.Next(children.Count)];
			}

			var newNode = new Node
			{
				Depth = chosen.Depth + 1,
				NodePower = random.Next(maxValue: 5),
				NodeTargets = new[] {(NodeTarget) random.Next(maxValue: 4)}
			};

			chosen.Children.Add(newNode);

			tree.InitTree();
			return tree;
		}
	}
}
