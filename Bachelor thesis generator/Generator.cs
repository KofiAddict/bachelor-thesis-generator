﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using Bachelor_thesis_generator.Common;
using Bachelor_thesis_generator.DataStructure;
using Bachelor_thesis_generator.Operators;
using MoreLinq;

namespace Bachelor_thesis_generator
{
	public class Generator
	{
		private static readonly Random Random = new Random(Seed: 142857);
		private static int[] _maxHealths;

		public static NodeTree GenerateNSGA(InputModel inputModel)
		{
			Console.WriteLine(value: "Running for new tree");
			_maxHealths = inputModel.PowerMarks.Values.Select(x => x.Sum(y => y.BaseHealth)).ToArray();
			var currentGeneration = GetRandomTrees();
			var ops = GetOperators(inputModel);
			for (var i = 0; i < inputModel.Generations; i++)
			{
				Console.WriteLine("Running generation " + i +" out of " + inputModel.Generations);
				var evolved = EvolveTree(currentGeneration, ops, inputModel);
				var rated = evolved.AsParallel().Select(x => RateTree(x, inputModel)).ToList();
				rated.AddRange(currentGeneration);
				// sorts trees so the first is the best
				rated = new List<RatedTree>(NonDominatedSorter.NonDominatedSort(rated));
				currentGeneration = rated.Take(inputModel.GenerationSize).ToList();
			}

			var final = currentGeneration.MaxBy(x => x.GraciousLossRating * inputModel.GraciousLossWeight +
			                                         x.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
			                                         x.BalanceRating * inputModel.BalanceWeight +
			                                         x.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
			                                         x.MostFrequentCaseRating * inputModel.MfcWeight +
			                                         x.WinRateRating * inputModel.WinRateWeight).First().Tree;
			final.InitTree();
			PostProcess(final, inputModel.PowerMarks.Keys.Max());
			final.InitTree();
			return final;
		}

		public static NodeTree GenerateEvo(InputModel inputModel)
		{
			Console.WriteLine(value: "Running for new tree");
			_maxHealths = inputModel.PowerMarks.Values.Select(x => x.Sum(y => y.BaseHealth)).ToArray();
			var currentGeneration = GetRandomTrees();
			var ops = GetOperators(inputModel);
			for (var i = 0; i < inputModel.Generations; i++)
			{
				Console.WriteLine("Running generation " + i + " out of " + inputModel.Generations);
				var evolved = EvolveTree(currentGeneration, ops, inputModel);
				var rated = evolved.AsParallel().Select(x => RateTree(x, inputModel)).ToList();
				rated.AddRange(currentGeneration);
				// sorts trees so the first is the best
				rated.Sort((tree, ratedTree) => tree.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
				                                tree.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
				                                tree.MostFrequentCaseRating * inputModel.MfcWeight +
				                                tree.WinRateRating * inputModel.WinRateWeight +
				                                tree.BalanceRating * inputModel.BalanceWeight +
				                                tree.GraciousLossRating * inputModel.GraciousLossWeight -
				                                (ratedTree.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
				                                 ratedTree.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
				                                 ratedTree.MostFrequentCaseRating * inputModel.MfcWeight +
				                                 ratedTree.WinRateRating * inputModel.WinRateWeight +
				                                 ratedTree.BalanceRating * inputModel.BalanceWeight +
				                                 ratedTree.GraciousLossRating * inputModel.GraciousLossWeight) > 0
					? -1
					: 1);
				currentGeneration = rated.Take(inputModel.GenerationSize).ToList();
			}

			var final = currentGeneration.MaxBy(x => x.GraciousLossRating * inputModel.GraciousLossWeight +
			                                         x.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
			                                         x.BalanceRating * inputModel.BalanceWeight +
			                                         x.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
			                                         x.MostFrequentCaseRating * inputModel.MfcWeight +
			                                         x.WinRateRating * inputModel.WinRateWeight).First().Tree;
			final.InitTree();
			PostProcess(final, inputModel.PowerMarks.Keys.Max());
			final.InitTree();
			return final;
		}

		public static NodeTree GenerateHillClimbing(InputModel inputModel)
		{
			Console.WriteLine(value: "Running for new tree");
			_maxHealths = inputModel.PowerMarks.Values.Select(x => x.Sum(y => y.BaseHealth)).ToArray();
			var currentGeneration = new List<RatedTree>()
			{
				new RatedTree
				{
					Tree = new NodeTree(new Node
					{
						Children = new List<Node>(),
						Id = 1,
						Parent = null,
						NodePower = 1,
						NodeTargets = new[] {NodeTarget.All},
						NodeState = NodeState.None,
						Depth = 0
					}),
					WinRateRating = double.NegativeInfinity
				}
			};
			var ops = GetOperators(inputModel);
			for (var i = 0; i < inputModel.Generations; i++)
			{
				Console.WriteLine("Running generation " + i + " out of " + inputModel.Generations);
				var evolved = EvolveTree(currentGeneration, ops, inputModel);
				var rated = evolved.AsParallel().Select(x => RateTree(x, inputModel)).ToList();
				rated.AddRange(currentGeneration);
				// sorts trees so the first is the best
				rated.Sort((tree, ratedTree) => tree.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
												tree.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
												tree.MostFrequentCaseRating * inputModel.MfcWeight +
												tree.WinRateRating * inputModel.WinRateWeight +
												tree.BalanceRating * inputModel.BalanceWeight +
												tree.GraciousLossRating * inputModel.GraciousLossWeight -
												(ratedTree.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
												 ratedTree.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
												 ratedTree.MostFrequentCaseRating * inputModel.MfcWeight +
												 ratedTree.WinRateRating * inputModel.WinRateWeight +
												 ratedTree.BalanceRating * inputModel.BalanceWeight +
												 ratedTree.GraciousLossRating * inputModel.GraciousLossWeight) > 0
					? -1
					: 1);
				currentGeneration = rated.Take(count: 1).ToList();
			}

			var final = currentGeneration.MaxBy(x => x.GraciousLossRating * inputModel.GraciousLossWeight +
													 x.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
													 x.BalanceRating * inputModel.BalanceWeight +
													 x.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
													 x.MostFrequentCaseRating * inputModel.MfcWeight +
													 x.WinRateRating * inputModel.WinRateWeight).First().Tree;
			final.InitTree();
			PostProcess(final, inputModel.PowerMarks.Keys.Max());
			final.InitTree();
			return final;
		}

		public static NodeTree GenerateRandomHillClimbing(InputModel inputModel)
		{
			Console.WriteLine(value: "Running for new tree");
			_maxHealths = inputModel.PowerMarks.Values.Select(x => x.Sum(y => y.BaseHealth)).ToArray();
			var results = new List<RatedTree>();
			var starts = GetRandomTrees();
			foreach (var start in starts)
			{
				var currentGeneration = new List<RatedTree>(){ start };
				var ops = GetOperators(inputModel);
				for (var i = 0; i < inputModel.Generations; i++)
				{
					Console.WriteLine("Running generation " + i + " out of " + inputModel.Generations + " run " + (starts.IndexOf(start) + 1) + "/" + starts.Count);
					var evolved = EvolveTree(currentGeneration, ops, inputModel);
					var rated = evolved.AsParallel().Select(x => RateTree(x, inputModel)).ToList();
					rated.AddRange(currentGeneration);
					// sorts trees so the first is the best
					rated.Sort((tree, ratedTree) => tree.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
													tree.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
													tree.MostFrequentCaseRating * inputModel.MfcWeight +
													tree.WinRateRating * inputModel.WinRateWeight +
													tree.BalanceRating * inputModel.BalanceWeight +
													tree.GraciousLossRating * inputModel.GraciousLossWeight -
													(ratedTree.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
													 ratedTree.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
													 ratedTree.MostFrequentCaseRating * inputModel.MfcWeight +
													 ratedTree.WinRateRating * inputModel.WinRateWeight +
													 ratedTree.BalanceRating * inputModel.BalanceWeight +
													 ratedTree.GraciousLossRating * inputModel.GraciousLossWeight) > 0
						? -1
						: 1);
					currentGeneration = rated.Take(count: 1).ToList();
				} 
				results.Add(currentGeneration.MaxBy(x => x.GraciousLossRating * inputModel.GraciousLossWeight +
				                                         x.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
				                                         x.BalanceRating * inputModel.BalanceWeight +
				                                         x.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
				                                         x.MostFrequentCaseRating * inputModel.MfcWeight +
				                                         x.WinRateRating * inputModel.WinRateWeight).First());
			}

			var final = results.MaxBy(x => x.GraciousLossRating * inputModel.GraciousLossWeight +
													 x.AverageWinTurnRating * inputModel.AverageWinTurnWeight +
													 x.BalanceRating * inputModel.BalanceWeight +
													 x.MedianWinTurnRating * inputModel.MedianWinTurnWeight +
													 x.MostFrequentCaseRating * inputModel.MfcWeight +
													 x.WinRateRating * inputModel.WinRateWeight).First().Tree;
			final.InitTree();
			PostProcess(final, inputModel.PowerMarks.Keys.Max());
			final.InitTree();
			return final;
		}

		private static List<IOperator> GetOperators(InputModel inputModel)
		{
			var list = new List<IOperator>
			{
				new AddNodeOp(new Random(Seed: 1), inputModel.AddNodeOpProb),
				new MergeNodesOp(new Random(Seed: 2), inputModel.MergeOpProb),
				new SplitNodeOp(new Random(Seed: 3), inputModel.SplitOpProb),
				new StrengthenOp(new Random(Seed: 4), inputModel.StrengthenOpProb),
				new WeakenOp(new Random(Seed: 5), inputModel.WeakenOpProb)
			};
			return list;
		}

		private static List<RatedTree> GetRandomTrees()
		{
			var random = new Random(Seed: 142857);
			var ratedTrees = new List<RatedTree>();

			for (var i = 0; i < 10; i++)
			{
				var tree = new RatedTree
				{
					Tree = new NodeTree(new Node
					{
						Children = new List<Node>(),
						Id = 1,
						Parent = null,
						NodePower = 1,
						NodeTargets = new[] {NodeTarget.All},
						NodeState = NodeState.None,
						Depth = 0
					}),
					WinRateRating = double.NegativeInfinity
				};

				var upper = random.Next(maxValue: 10);
				for (var j = 0; j < upper; j++)
				{
					var parent = tree.Tree.NodeList[random.Next(tree.Tree.NodeList.Count)];

					var targets = new[] {(NodeTarget) random.Next(maxValue: 4)};

					var node = new Node
					{
						Children = new List<Node>(),
						Id = tree.Tree.NodeList.Count + 1,
						Parent = parent,
						NodePower = 1,
						NodeTargets = targets,
						NodeState = NodeState.None,
						Depth = parent.Depth + 1
					};

					parent.Children.Add(node);
					tree.Tree.NodeList.Add(node);
				}

				ratedTrees.Add(tree);
			}

			return ratedTrees;
		}

		private static void PostProcess(NodeTree tree, int maxPoints)
		{
			var toRemove = new List<Node>();
			foreach (var node in tree.NodeList)
				if ((node.Children == null || node.Children.Count == 0) && node.NodePower == 0)
				{
					node.Parent.Children.Remove(node);
					toRemove.Add(node);
				}

			foreach (var node in toRemove) tree.NodeList.Remove(node);
			tree.InitTree();

			tree.Root.Children.Add(new Node()
			{
				NodePower = 1,
				NodeTargets = new[] { NodeTarget.Damage }
			}); tree.Root.Children.Add(new Node()
			{
				NodePower = 2,
				NodeTargets = new[] { NodeTarget.Damage }
			});
			var queue = new Queue<Node>(new[] {tree.Root});
			while (queue.Count > 0)
			{
				var node = queue.Dequeue();
				var dic = new Dictionary<NodeTarget[], int>(new TargetsComparer());
				foreach (var nodeChild in node.Children.Where(x => x.Children == null || x.Children.Count == 0).ToList())
				{
					if (dic.ContainsKey(nodeChild.NodeTargets))
					{
						dic[nodeChild.NodeTargets]++;
					}
					else
					{
						dic.Add(nodeChild.NodeTargets, 1);
					}
				}

				foreach (var i in dic.Keys)
				{
					if (dic[i] > 1)
					{
						var list = node.Children.Where(x => x.NodeTargets.SequenceEqual(i)).ToList();

						foreach (var node1 in list)
						{
							node.Children.Remove(node1);
						}

						list = list.OrderByDescending(x => x.NodePower).ToList();

						var nodeRef = node;
						foreach (var node1 in list)
						{
							nodeRef.Children.Add(node1);
							nodeRef = node1;
						}
					}
				}

				foreach (var nodeChild in node.Children) queue.Enqueue(nodeChild);
			}

			tree.InitTree();
		}

		private static void ConvertSubtreeToPath(Node node)
		{
			var queue = new Queue<Node>(new[] {node});
			var nodes = new List<Node>();
			if (node.Children.Count == 0)
			{
				return;
			}
			while (queue.Count > 0)
			{
				var poppedNode = queue.Dequeue();
				poppedNode.Children.ForEach(x => queue.Enqueue(x));
				nodes.Add(poppedNode);
			}

			nodes.RemoveAt(index: 0);

			nodes = nodes.OrderByDescending(x => x.NodePower).ToList();

			foreach (var node1 in nodes)
			{
				node.Children = new List<Node>(new[] {node1});
				node1.Parent = node;
				node = node1;
			}

			nodes[nodes.Count - 1].Children = new List<Node>();
		}

		/// <summary>
		///     Gets count of nodes in subtree including given root
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		private static int GetSubtreeCount(Node node)
		{
			var count = 0;
			var queue = new Queue<Node>(new[] {node});
			while (queue.Count > 0)
			{
				var poppedNode = queue.Dequeue();
				poppedNode.Children.ForEach(x => queue.Enqueue(x));
				count++;
			}

			return count;
		}

		private static RatedTree RateTree(NodeTree nodeTree, InputModel inputModel)
		{
			//Console.WriteLine("Rating tree with " + nodeTree.NodeList.Count + " nodes.");

			#region Simulation

			inputModel = inputModel.Clone();
			var totalResults = new List<List<List<int>>>();
			foreach (var deck in ReferenceCardSets.Decks.Select(x => x.Select(y => y.Clone()).ToList()).ToList()) // thread safety, deep copying data
			{
				var nodeConfigs = new List<List<Node>> {new List<Node> {nodeTree.Root}};
				var deckResults = new List<List<int>>();
				foreach (var i in inputModel.PowerMarks.Keys)
				{
					var enemies = inputModel.PowerMarks[i];
					var combinations = new List<List<Node>>();
					if (nodeTree.NodeList.Count > i)
						foreach (var nodeConfig in nodeConfigs)
							GetPossibleCombinations(nodeConfig, combinations, i, nodeConfig.Count - 1);
					else
						combinations.Add(nodeTree.NodeList.ToList());

					combinations = combinations.Distinct(new NodeListComparer()).ToList();

					var simulationResults = new List<int>();
					var successfulChoices = new List<List<Node>>();
					if (combinations.Count == 0) combinations.Add(new List<Node>());
					foreach (var combination in combinations)
					{
						combination.ForEach(x => x.ApplyEffectToCards(deck));
						var result = Simulate(deck, enemies);
						if (result > 0) successfulChoices.Add(combination);
						if (result == 0)
						{
							throw new Exception();
						}

						simulationResults.Add(result);

						deck.ForEach(x => x.Reset());
						enemies.ForEach(x => x.Reset());
					}

					deckResults.Add(simulationResults);
					nodeConfigs = successfulChoices;
				}

				totalResults.Add(deckResults);
			}

			#endregion

			var decksWinRateRating = new List<double>();
			var decksAverageWinTurnRating = new List<double>();
			var decksMedianWinTurnRating = new List<double>();
			var decksMfcWinTurnRating = new List<double>();
			var decksGraciousLossRating = new List<double>();
			foreach (var deckEncountersResults in totalResults)
			{
				var listOfWinRateRatings = new List<double>();
				var listOfAverageWinTurnRatings = new List<double>();
				var listOfMedianWinTurnRatings = new List<double>();
				var listOfMfcWinTurnRatings = new List<double>();
				var listOfGraciousLossRatings = new List<double>();
				for (var index = 0; index < deckEncountersResults.Count; index++)
				{
					var encounterCombinationsResults = deckEncountersResults[index];
					var winRateRating = 1d;
					var averageWinTurnRating = 0d;
					var medianWinTurnRating = 0d;
					var mfcWinTurnRating = 0d;
					var graciousLossRating = 1d;

					graciousLossRating -= encounterCombinationsResults.Any(x => x < 0)
						? -encounterCombinationsResults.Where(x => x < 0).Sum() /
						  (double)(_maxHealths[index] * encounterCombinationsResults.Count(x => x < 0))
						: 0.5;

					var numOfWon = encounterCombinationsResults.Count(x => x > 0);
					if (numOfWon == 0)
					{
						winRateRating = -1d;
						winRateRating += 1d / -encounterCombinationsResults.Sum();
						listOfWinRateRatings.Add(winRateRating);
						listOfAverageWinTurnRatings.Add(averageWinTurnRating);
						listOfMedianWinTurnRatings.Add(medianWinTurnRating);
						listOfMfcWinTurnRatings.Add(mfcWinTurnRating);
						listOfGraciousLossRatings.Add(graciousLossRating);
						continue;
					}

					var winRatio = (double) numOfWon / encounterCombinationsResults.Count;

					if (winRatio < 0.7)
						winRateRating = winRatio / 0.7;
					else if (winRatio > 0.7) winRateRating = (1 - winRatio) / 0.7;

					var medianTurnsToWin =
						encounterCombinationsResults.Where(x => x > 0).OrderBy(x => x).ToList()[numOfWon / 2];

					if (medianTurnsToWin < 3)
					{
						medianWinTurnRating = medianTurnsToWin / 3d;
					}
					else if (medianTurnsToWin > 5)
					{
						var temp = 1 - medianTurnsToWin * 0.2;
						medianWinTurnRating = temp < 0 ? 0 : temp;
					}

					var averageTurnsToWin = encounterCombinationsResults.Where(x => x > 0).Average();

					if (averageTurnsToWin < 3)
					{
						averageWinTurnRating = averageTurnsToWin / 2d;
					}
					else if (averageTurnsToWin > 5)
					{
						var temp = 1 - averageTurnsToWin * 0.2;
						averageWinTurnRating = temp < 0 ? 0 : temp;
					}

					var mfc = encounterCombinationsResults.Where(x => x > 0).GroupBy(x => x).MaxBy(x => x.Count())
						.First().Key;

					if (mfc < 3)
					{
						mfcWinTurnRating = mfc / 2d;
					}
					else if (mfc > 5)
					{
						var temp = 1 - mfc * 0.2;
						mfcWinTurnRating = temp < 0 ? 0 : temp;
					}

					

					listOfWinRateRatings.Add(winRateRating);
					listOfAverageWinTurnRatings.Add(averageWinTurnRating);
					listOfMedianWinTurnRatings.Add(medianWinTurnRating);
					listOfMfcWinTurnRatings.Add(mfcWinTurnRating);
					listOfGraciousLossRatings.Add(graciousLossRating);
				}

				decksWinRateRating.Add(
					1 - listOfWinRateRatings.RootMeanSquare(Enumerable.Repeat(element: 1d, count: listOfWinRateRatings.Count)
						.ToList()));
				decksAverageWinTurnRating.Add(
					1 - listOfAverageWinTurnRatings.RootMeanSquare(Enumerable
						.Repeat(element: 1d, count: listOfAverageWinTurnRatings.Count).ToList()));
				decksMedianWinTurnRating.Add(
					1 - listOfMedianWinTurnRatings.RootMeanSquare(Enumerable
						.Repeat(element: 1d, count: listOfMedianWinTurnRatings.Count).ToList()));
				decksMfcWinTurnRating.Add(
					1 - listOfMfcWinTurnRatings.RootMeanSquare(Enumerable.Repeat(element: 1d, count: listOfMfcWinTurnRatings.Count)
						.ToList()));
				decksGraciousLossRating.Add(
					1 - listOfGraciousLossRatings.RootMeanSquare(Enumerable.Repeat(element: 1d, count: listOfGraciousLossRatings.Count)
						.ToList()));
			}

			var treeBalanceRating = GetTreeBalanceRating(nodeTree.Root, weight: 0.5);

			return new RatedTree
			{
				Tree = nodeTree,
				MedianWinTurnRating =
					1 - decksMedianWinTurnRating.RootMeanSquare(Enumerable.Repeat(element: 1d, count: decksMedianWinTurnRating.Count)
						.ToList()),
				AverageWinTurnRating =
					1 - decksAverageWinTurnRating.RootMeanSquare(Enumerable.Repeat(element: 1d, count: decksAverageWinTurnRating.Count)
						.ToList()),
				MostFrequentCaseRating =
					1 - decksMfcWinTurnRating.RootMeanSquare(
						Enumerable.Repeat(element: 1d, count: decksMfcWinTurnRating.Count).ToList()),
				WinRateRating =
					1 - decksWinRateRating.RootMeanSquare(Enumerable.Repeat(element: 1d, count: decksWinRateRating.Count).ToList()),
				GraciousLossRating =
					1 - decksGraciousLossRating.RootMeanSquare(Enumerable.Repeat(element: 1d, count: decksWinRateRating.Count).ToList()),
				BalanceRating = treeBalanceRating
			};
		}

		private static double GetTreeBalanceRating(Node node, double weight)
		{
			if (node.Children == null || node.Children.Count == 0)
			{
				return weight;
			}

			var subTreeCounts = node.Children.Select(x => GetSubtreeCount(node)).ToList();
			var min = subTreeCounts.Min();
			var max = subTreeCounts.Max();
			var rating = (1 - (max - min) / max) * weight;

			var childrenRatings =
				node.Children.Select(x => GetTreeBalanceRating(x, weight / (2 * node.Children.Count)));

			rating += childrenRatings.Sum();

			return rating;
		}

		/// <summary>
		///     Simulates synthetically the gameplay
		/// </summary>
		/// <param name="deck"></param>
		/// <param name="enemies"></param>
		/// <returns>Number of turns needed to kill enemies, or negative of sum of remaining health of the enemy</returns>
		private static int Simulate(IList<Card> deck, List<Enemy> enemies)
		{
			enemies = enemies.OrderByDescending(x => x.Attack / x.Health).ToList();
			var damageAverages = new List<double>();
			var aoeDamageAverage = 0d;
			var totalManaCost = 0d;
			var totalEnergyCost = 0d;
			var manaMultiplier = 1d;
			var energyMultiplier = 1d;
			foreach (var card in deck)
			{
				switch (card.CostType)
				{
					case CostType.Mana:
						totalManaCost += card.Cost / 3d;
						break;
					case CostType.Energy:
						totalEnergyCost += card.Cost / 3d;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				if (totalEnergyCost > 20) energyMultiplier = 20d / totalEnergyCost;

				if (totalManaCost > 10) manaMultiplier = 10d / totalManaCost;
			}

			foreach (var card in deck)
				switch (card.CardType)
				{
					case CardType.TargetedEffect:
						switch (card.CostType)
						{
							case CostType.Mana:
								damageAverages.Add(card.EffectValue * manaMultiplier / 3d);
								break;
							case CostType.Energy:
								damageAverages.Add(card.EffectValue * energyMultiplier / 3d);
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}

						break;
					case CardType.NonTargetedEffect:
						switch (card.CostType)
						{
							case CostType.Mana:
								aoeDamageAverage += card.EffectValue * manaMultiplier / 3d;
								break;
							case CostType.Energy:
								aoeDamageAverage += card.EffectValue * energyMultiplier / 3d;
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}

						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

			var health = 50;
			var turn = 1;

			while (true)
			{
				enemies.ForEach(x => x.Health -= aoeDamageAverage);

				foreach (var damageAverage in damageAverages)
				foreach (var enemy in enemies)
					if (enemy.Health > 0)
					{
						enemy.Health -= damageAverage;
						break;
					}

				if (enemies.All(x => x.Health <= 0)) return turn;
				++turn;

				foreach (var enemy in enemies)
					if (enemy.Health > 0)
						health -= enemy.Attack;

				if (health <= 0) return -((int) enemies.Sum(x => x.Health < 0 ? 0 : x.Health) + 1);
			}
		}

		private static void GetPossibleCombinations(List<Node> current,
			ICollection<List<Node>> combinations,
			int maxSteps, int step = 0)
		{
			if (step == maxSteps)
			{
				combinations.Add(current);
				return;
			}

			var possibleSelections =
				current.Select(x => x.Children).SelectMany(x => x).Distinct().Except(current).ToList();

			foreach (var node in possibleSelections)
			{
				var currentWithNew = new List<Node>(current) {node};
				GetPossibleCombinations(currentWithNew, combinations, maxSteps, step + 1);
			}
		}

		private static bool UnorderedEqual<T>(ICollection<T> a, ICollection<T> b)
		{
			if (a.Count != b.Count) return false;

			var d = new Dictionary<T, int>();
			foreach (var item in a)
			{
				if (d.TryGetValue(item, out var c))
					d[item] = c + 1;
				else
					d.Add(item, value: 1);
			}

			foreach (var item in b)
			{
				if (d.TryGetValue(item, out var c))
				{
					if (c == 0) return false;
					d[item] = c - 1;
				}
				else
				{
					return false;
				}
			}

			foreach (var v in d.Values)
				if (v != 0)
					return false;

			return true;
		}

		private static List<NodeTree> EvolveTree(IReadOnlyList<RatedTree> bestTrees, IReadOnlyCollection<IOperator> operators, InputModel inputModel)
		{
			var toReturn = (from tree in bestTrees from op in operators let copy = tree.Tree.Clone() select op.Mutate(copy, inputModel)).ToList();

			toReturn.RemoveAll(x => x == null);

			if (bestTrees.Count > 1)
			{
				for (var i = 0; i < inputModel.GenerationSize; i++)
					toReturn.Add(Crossover(bestTrees[Random.Next(bestTrees.Count)].Tree.Clone(),
						bestTrees[Random.Next(bestTrees.Count)].Tree.Clone())); 
			}

			toReturn = toReturn.Where(y =>
				y.NodeList.All(x => x.Children.Count <= inputModel.PowerMarks.Keys.Max() - x.Depth)).ToList();
			toReturn = toReturn.Where(x => x.NodeList.Max(y => y.Depth) <= inputModel.PowerMarks.Keys.Max()).ToList();
			toReturn = toReturn.Where(x => x.NodeList.Count <= inputModel.NodeCount).ToList();

			return toReturn;
		}

		private static NodeTree Crossover(NodeTree one, NodeTree two)
		{
			var mergeNode = MergeNode(one.Clone().Root, two.Clone().Root);
			var newTree = new NodeTree(mergeNode);
			newTree.InitTree();

			return newTree;
		}

		private static Node MergeNode(Node one, Node two)
		{
			var newNode = new Node
			{
				Children = new List<Node>(),
				Depth = one.Depth,
				NodeState = NodeState.None,
				NodeTargets = Random.NextDouble() > 0.5 ? one.NodeTargets.ToArray() : two.NodeTargets.ToArray(),
				NodePower = Random.NextDouble() > 0.5 ? (one.NodePower + two.NodePower) / 2 : Random.NextDouble() > 0.5 ? one.NodePower : two.NodePower
			};

			var maxChildren = one.Children.Count > two.Children.Count ? one.Children.Count : two.Children.Count;
			for (var i = 0; i < maxChildren; i++)
			{
				if (one.Children.Count <= i)
				{
					if (Random.NextDouble() > 0.5)
						newNode.Children.Add(two.Children[i]);
					continue;
				}

				if (two.Children.Count <= i)
				{
					if (Random.NextDouble() > 0.5) 
						newNode.Children.Add(one.Children[i]);
					continue;
				}

				var mergeNode = MergeNode(one.Children[i], two.Children[i]);
				newNode.Children.Add(mergeNode);
				mergeNode.Parent = newNode;
			}

			return newNode;
		}

		internal class RatedTree
		{
			public NodeTree Tree;

			public double AverageWinTurnRating;
			public double MedianWinTurnRating;
			public double MostFrequentCaseRating;
			public double WinRateRating;
			public double BalanceRating;
			public double GraciousLossRating;
		}

		internal class NodeListComparer : IEqualityComparer<List<Node>>
		{
			public bool Equals(List<Node> x, List<Node> y)
			{
				return UnorderedEqual(x, y);
			}

			public int GetHashCode(List<Node> obj)
			{
				return obj.Sum(x => x.GetHashCode()) + obj.Select(node => node.Id).Aggregate((i, i1) => i1 * i);
			}
		}

		public class TargetsComparer : IEqualityComparer<NodeTarget[]>
		{
			public bool Equals(NodeTarget[] x, NodeTarget[] y)
			{
				if (x.Length != y.Length)
				{
					return false;
				}
				for (int i = 0; i < x.Length; i++)
				{
					if (x[i] != y[i])
					{
						return false;
					}
				}
				return true;
			}

			public int GetHashCode(NodeTarget[] obj)
			{
				int result = 17;
				for (int i = 0; i < obj.Length; i++)
				{
					unchecked
					{
						result = (int) (result * 23 + obj[i]);
					}
				}
				return result;
			}
		}
	}
}