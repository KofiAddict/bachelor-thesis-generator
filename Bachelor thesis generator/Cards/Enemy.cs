﻿using System;

namespace Bachelor_thesis_generator.Cards
{
	public class Enemy
	{
		public int Attack;
		public double Health;
		public int BaseAttack;
		public int BaseHealth;
		public Guid Guid = Guid.NewGuid();
		public int ThreatValue;

		public void Reset()
		{
			Attack = BaseAttack;
			Health = BaseHealth;
		}

		public Enemy Clone()
		{
			return new Enemy
			{
				Health = Health,
				Attack = Attack,
				BaseAttack = BaseAttack,
				BaseHealth = BaseHealth,
				ThreatValue = ThreatValue,
				Guid = Guid
			};
		}
	}
}
