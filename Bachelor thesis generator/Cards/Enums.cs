﻿namespace Bachelor_thesis_generator.Cards
{
	public enum CardType
	{
		/// <summary>
		/// One time targeted effect
		/// </summary>
		TargetedEffect,

		/// <summary>
		/// One time non targeted effect
		/// </summary>
		NonTargetedEffect
	}

	public enum CostType
	{
		Mana,
		Energy
	}

	public enum EffectType
	{
		Heal,
		Damage,
		ReduceDamage
	}

	public enum NodeTarget
	{
		SingleTarget,
		MultiTarget,
		Energy,
		Mana,
		Heal,
		Damage,
		DamageReduction,
		All
	}

	public enum NodeState
	{
		None
	}
}