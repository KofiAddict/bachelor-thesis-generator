﻿namespace Bachelor_thesis_generator.Cards
{
	public class Card
	{
		public virtual CardType CardType { get; set; }
		public int Cost;
		public CostType CostType;
		public EffectType CardEffectType;
		public int BaseEffectValue;
		public int BaseCardCost;
		public EffectType BaseEffectType;
		public int EffectValue;

		public void Reset()
		{
			Cost = BaseCardCost;
			EffectValue = BaseEffectValue;
			CardEffectType = BaseEffectType;
		}

		public Card Clone()
		{
			return new Card
			{
				BaseCardCost = BaseCardCost,
				BaseEffectType = BaseEffectType,
				BaseEffectValue = BaseEffectValue,
				CardEffectType = CardEffectType,
				CardType = CardType,
				Cost = Cost,
				CostType = CostType,
				EffectValue = EffectValue
			};
		}

		public bool IsEqual(Card card)
		{
			return this.BaseCardCost == card.BaseCardCost &&
			       this.BaseEffectType == card.BaseEffectType &&
			       this.BaseEffectValue == card.BaseEffectValue &&
			       this.CardEffectType == card.CardEffectType &&
			       this.CardType == card.CardType &&
			       this.Cost == card.Cost &&
			       this.CostType == card.CostType &&
			       this.EffectValue == card.EffectValue;
		}
	}
}
