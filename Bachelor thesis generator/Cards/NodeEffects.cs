﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bachelor_thesis_generator.Cards
{
	public static class NodeEffects
	{
		public static Action<IEnumerable<Card>> ChangeFlatEffectAction(int amount, NodeTarget[] restrictions)
		{
			var combinedPredicate = restrictions.Length == 0
				? card => true
				: restrictions.Select(GetPredicate)
					.Aggregate((predicate, predicate1) => card => predicate(card) && predicate1(card));

			return cards =>
			{
				foreach (var card in cards.Where(x => combinedPredicate(x)))
				{
					card.EffectValue += amount;
				}
			};
		}

		public static Predicate<Card> GetPredicate(NodeTarget restriction)
		{
			switch (restriction)
			{
				case NodeTarget.SingleTarget:
					return card => card.CardType == CardType.TargetedEffect;
				case NodeTarget.MultiTarget:
					return card => card.CardType == CardType.NonTargetedEffect;
				case NodeTarget.Energy:
					return card => card.CostType == CostType.Energy;
				case NodeTarget.Mana:
					return card => card.CostType == CostType.Mana;
				case NodeTarget.Heal:
					return card => card.CardEffectType == EffectType.Heal;
				case NodeTarget.Damage:
					return card => card.CardEffectType == EffectType.Damage;
				case NodeTarget.All:
					return card => true;
				case NodeTarget.DamageReduction:
					return card => card.CardEffectType == EffectType.ReduceDamage;
				default:
					throw new ArgumentOutOfRangeException(nameof(restriction), restriction, message: null);
			}
		}
	}
}
