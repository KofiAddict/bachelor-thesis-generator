﻿using System;
using System.Collections.Generic;

namespace Bachelor_thesis_generator.Common
{
	public static class Extensions
	{
		public static double RootMeanSquare(this IList<double> values, IList<double> expected)
		{
			double sumSq = 0;

			if (values.Count != expected.Count)
			{
				throw new ArgumentException();
			}

			for (var i = 0; i < values.Count; ++i)
			{
					var p1 = values[i];
					var p2 = expected[i];
					var err = p2 - p1;
					sumSq += err * err;
			}

			var mse = sumSq / values.Count;
			return mse;
		}
	}
}
