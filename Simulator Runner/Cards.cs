﻿using System.Collections.Generic;
using Bachelor_thesis_generator.Cards;

namespace Simulator_Runner
{
	class Cards
	{
		public static List<Card> cards = new List<Card>();

		static Cards()
		{
			var singleTargetAttack1 = new Card
			{
				Cost = 4,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 6,
				BaseCardCost = 4,
				BaseEffectType = (EffectType)1,
				EffectValue = 6
			};

			var singleTargetAttack2 = new Card
			{
				Cost = 7,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 9,
				BaseCardCost = 7,
				BaseEffectType = (EffectType)1,
				EffectValue = 9
			};

			var singleTargetSpell1 = new Card
			{
				Cost = 2,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 5,
				BaseCardCost = 2,
				BaseEffectType = (EffectType)1,
				EffectValue = 5
			};

			var singleTargetSpell2 = new Card
			{
				Cost = 8,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 20,
				BaseCardCost = 8,
				BaseEffectType = (EffectType)1,
				EffectValue = 20
			};

			var aoeSpell1 = new Card
			{
				Cost = 4,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 4,
				BaseCardCost = 5,
				BaseEffectType = (EffectType)1,
				EffectValue = 4,
				CardType = CardType.NonTargetedEffect
			};

			var aoeSpell2 = new Card
			{
				Cost = 4,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 4,
				BaseCardCost = 5,
				BaseEffectType = (EffectType)1,
				EffectValue = 4,
				CardType = CardType.NonTargetedEffect
			};

			var aoeAttack1 = new Card
			{
				Cost = 6,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 2,
				BaseCardCost = 6,
				BaseEffectType = (EffectType)1,
				EffectValue = 2,
				CardType = CardType.NonTargetedEffect
			};

			var aoeAttack2 = new Card
			{
				Cost = 15,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 6,
				BaseCardCost = 15,
				BaseEffectType = (EffectType)1,
				EffectValue = 6,
				CardType = CardType.NonTargetedEffect
			};

			var differentCards = new List<Card>
			{
				aoeAttack2,
				aoeAttack1,
				singleTargetAttack2,
				singleTargetAttack1,
				singleTargetSpell1,
				singleTargetSpell2,
				aoeSpell1,
				aoeSpell2
			};

			cards.Add(singleTargetAttack1);
			cards.Add(singleTargetAttack1);
			cards.Add(singleTargetAttack1);
			cards.Add(singleTargetAttack1);
			cards.Add(singleTargetAttack2);
			cards.Add(singleTargetSpell1);
			cards.Add(singleTargetSpell1);
			cards.Add(singleTargetSpell1);
			cards.Add(singleTargetSpell1);
			cards.Add(singleTargetSpell2);
			cards.Add(aoeAttack1);
			cards.Add(aoeAttack1);
			cards.Add(aoeAttack2);
			cards.Add(aoeSpell1);
			cards.Add(aoeSpell1);
		}
	}
}
