﻿using System;

namespace Generator_runner
{
	[Serializable]
	internal class EnemySetEntity
	{
		public EnemyEntity[] Entities;
	}

	[Serializable]
	internal class EnemyEntity
	{
		public int Attack;
		public int Defense;
	}
}